#ifndef AUDIO_H
#define AUDIO_H

#include "base.h"

typedef enum SoundId {
    SOUND_SPLASH_SCREEN,
    SOUND_EXPLODED,
    SOUND_FINISHED,
    SOUND_DIAMOND_1,
    SOUND_DIAMOND_2,
    SOUND_DIAMOND_3,
    SOUND_DIAMOND_4,
    SOUND_DIAMOND_5,
    SOUND_DIAMOND_6,
    SOUND_DIAMOND_7,
    SOUND_DIAMOND_8,
} SoundId;

SDL_AudioDeviceID init_audio();
void play_sound(SoundId);
void play_looped_sound(SoundId);
void stop_looped_sounds();

#endif  // AUDIO_H
