#include "audio.h"
#include "base.h"

#define STB_IMAGE_IMPLEMENTATION
#include "lib/stb_image.h"

// ============================= Globals ==========================================================

const int SCREEN_HEIGHT = 500;
const int SCREEN_WIDTH = 750;

double gPerformanceFrequency;

// ============================= Functions ========================================================

u64 time_now() { return SDL_GetPerformanceCounter(); }

double seconds_since(u64 timestamp) {
    return (double)(time_now() - timestamp) / gPerformanceFrequency;
}

SDL_Texture *load_texture(const char *filename, SDL_Renderer *renderer) {
    SDL_Texture *texture;
    int width, height, num_channels;
    void *pixels = stbi_load(filename, &width, &height, &num_channels, 0);

    SDL_Rect rect = {0, 0, width, height};

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STATIC, width,
                                height);
    if (texture == NULL) {
        printf("Couldn't create texture: %s\n", SDL_GetError());
        return NULL;
    }

    int result = SDL_UpdateTexture(texture, &rect, pixels, width * num_channels);

    if (result != 0) {
        printf("Couldn't update texture: %s\n", SDL_GetError());
        return NULL;
    }

    return texture;
}

// ============================= Main =============================================================

int main(int argc, char *args[]) {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        printf("SDL could not initialise! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }
    gPerformanceFrequency = (double)SDL_GetPerformanceFrequency();

    // Audio
    SDL_AudioDeviceID audio_device_id = init_audio();
    if (audio_device_id == 0) {
        printf("Couldn't init audio\n");
        return 1;
    }

    SDL_Window *window =
        SDL_CreateWindow("Boulder-Dash", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 960,
                         480, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    if (window == NULL) {
        printf("Couldn't create window: %s\n", SDL_GetError());
        return 1;
    }

    int window_width, window_height;
    SDL_GetWindowSize(window, &window_width, &window_height);

    SDL_Renderer *renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) {
        printf("Couldn't create renderer: %s\n", SDL_GetError());
        return 1;
    }

    // Load textures
    SDL_Texture *texture = load_texture("assets/img/sprites.png", renderer);

    SDL_CloseAudioDevice(audio_device_id);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
