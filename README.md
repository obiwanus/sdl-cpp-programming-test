# README #

## Instructions ##

### Breakout ###
We are going old school and want to test your skills at creating a Breakout game.  There are a number of breakout style games available on the internet for you to test out.
Don't spend too long on this, we want you to try and get as far as you can within 3-4 hours.

### Functional Requirements ###
1. The ball should rebound off the sides, top of the game board and off the paddle. 
2. If the ball falls off the bottom of the board the game is over or a life is lost.
3. The game should play from an SDL Window.
4. The game should be re-playable without closing the game.

### What we're looking for ###
* The process you used to complete this test
* Prudent use of Object Orientated design
* Code reusability
* Extensibility and maintainability of the software
* Use of appropriate SDL capabilities
* Use of best practices
* Your creativity in making the game fun

### Deliverables ###
* Instructions on how to run and play your game.
* Code should be committed to a git repository hosted on [bitbucket.org](https://bitbucket.org).
* A build for windows of your completed game committed to your repo.

## How do I get set up? ##
* [Fork](../../fork) this repository to your bitbucket account, then clone it to your local machine
* Ensure the Visual Studio project files work on your machine.
	* You should be able to build and run the game (window opens for 2sec then closes).

## Next Steps ##
After you have finished your submission, make sure the reviewers have read access to your repository. Our developers will review your submission and get back to you.